import React, { Component } from 'react';
import './App.css';
import ScientificMode from './component/ScientificMode';
import Operator from './component/Operator';
import DisplayEvaluate from './component/DisplayEvaluate';
import Keypad from './component/Keypad';
import Theme from './component/Theme';


class App extends Component {
  constructor() {
    super();
    this.state = {
      result: "",
      storeNumber: "",
      operator: "",
      operandScintific: "",
      theme: "light-theme"
    }
  }

  keyPress = keystroke => {

    if (keystroke === "=") {
      this.calculate();
    }
    else if (keystroke === "+" || keystroke === "-" || keystroke === "*" || keystroke === "/") {
      this.operator(keystroke);
    }
    else if (keystroke === "scintific") {

    }
    else if (keystroke === "square" || keystroke === "squareroot" || keystroke === "signinverse") {
      this.scintific(keystroke);
    }
    else if (keystroke === "clear") {
      this.reset();
    }
    else {
      this.setState({
        result: this.state.result + keystroke
      });
    }

  };


  calculate = () => {
    if (this.state.operator === "+") {
      this.setState({
        result: parseInt(this.state.result) + parseInt(this.state.storeNumber)
      });
    }
    else if (this.state.operator === "-") {
      this.setState({
        result: parseInt(this.state.storeNumber) - parseInt(this.state.result)
      });
    }
    else if (this.state.operator === "*") {
      this.setState({
        result: parseInt(this.state.result) * parseInt(this.state.storeNumber)
      });
    }
    else if (this.state.operator === "/") {
      this.setState({
        result: parseInt(this.state.storeNumber) / parseInt(this.state.result)
      });
    }
    else if (this.state.operator === "squareroot") {
      this.setState({
        result: Math.sqrt((this.state.result))
      });
    }

    this.setState({
      operator: ""
    });
    this.setState({
      storeNumber: ""
    });

  };

  scintific = (keystroke) => {
    if (keystroke === "signinverse") {
      this.setState({
        result: -1 * this.state.result
      });
      this.setState({
        operandScintific: "*-1"
      });
    }
    else if (keystroke === "square") {
      if (this.state.result.length === 0) {
        this.setState({
          result: "ERROR"
        });
      } else {
        this.setState({
          result: this.state.result * this.state.result
        });
        this.setState({
          operandScintific: "(side)²"
        });
      }
    }
    else if (keystroke === "squareroot") {
      if (this.state.result.length > 0) {
        this.setState({
          result: "ERROR"
        });
      } else {
        this.setState({
          operandScintific: "√"
        });
        this.setState({
          operator: "squareroot"
        });
      }
    }
  }

  operator = (operatorPass) => {
    this.setState({
      operator: operatorPass
    });
    this.setState({
      storeNumber: this.state.result
    });
    this.setState({
      operandScintific: this.state.result
    });
    this.setState({
      result: ""
    });
  }

  reset = () => {
    this.setState({
      result: ""
    });
    this.setState({
      storeNumber: ""
    });
    this.setState({
      operator: ""
    });
    this.setState({
      operandScintific: ""
    });
  };


  themeChange = (theme) => {
    
    if (theme === "light-theme") {
      this.setState({
        theme: "light-theme"
      });
      document.getElementsByClassName("App")[0].style.backgroundColor = '#fff';
      document.getElementsByClassName("App")[0].style.color = '#000';
      
      for(var i=0;i<document.getElementsByClassName("theme-button").length;i++){
        document.getElementsByClassName("theme-button")[i].style.backgroundColor = '#f0f0f0';
        document.getElementsByClassName("theme-button")[i].style.color = '#000';
      }
    }
    else if (theme === "dark-theme") {
      this.setState({
        theme: "dark-theme"
      });
      document.getElementsByClassName("App")[0].style.backgroundColor = '#000';
      document.getElementsByClassName("App")[0].style.color = '#fff';
      
      for(var j=0;j<document.getElementsByClassName("theme-button").length;j++){
        document.getElementsByClassName("theme-button")[j].style.backgroundColor = '#666';
        document.getElementsByClassName("theme-button")[j].style.color = '#fff';
      }
    }

  };

  render() {
    return (
      <div className="App light-theme">
        <h1>Calculator</h1>
        <Theme onClick={this.themeChange}></Theme>
        
        <div className="display-scintific">
          <ScientificMode operandScintific={this.state.operandScintific}></ScientificMode>
          <Operator operatorPerform={this.state.operator}></Operator>
          <DisplayEvaluate result={this.state.result}></DisplayEvaluate>
        </div>
        
        <Keypad onClick={this.keyPress}></Keypad>
      </div>
    );
  }

}

export default App;
