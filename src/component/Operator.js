import React, {Component} from 'react';

class Operator extends Component {

  render() {
    let {operatorPerform} = this.props;
    return (
      <div className="scientific">
        <p>{operatorPerform}</p>
      </div>
    );
  }

}


export default Operator;