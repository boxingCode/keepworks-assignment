import React, {Component} from 'react';

class Theme extends Component {

  render() {
    return (
      <div className="theme">
        <button className="theme-light" name="light-theme" onClick={e => this.props.onClick(e.target.name)}>Light Theme</button>
        <button className="theme-dark" name="dark-theme" onClick={e => this.props.onClick(e.target.name)}>Dark Theme</button>
      </div>
    );
  }

}


export default Theme;