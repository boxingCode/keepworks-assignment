import React, { Component } from 'react';

class Keypad extends Component {

  render() {
    return (
      <div className="button">
        <button name="1" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>1</button>
        <button name="2" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>2</button>
        <button name="3" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>3</button>
        <button name="+" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>+</button><br />

        <button name="4" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>4</button>
        <button name="5" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>5</button>
        <button name="6" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>6</button>
        <button name="-" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>-</button><br />

        <button name="7" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>7</button>
        <button name="8" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>8</button>
        <button name="9" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>9</button>
        <button name="*" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>x</button><br />

        <button name="clear" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>C</button>
        <button name="0" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>0</button>
        <button name="=" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>=</button>
        <button name="/" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>÷</button><br />

        <button name="signinverse" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>!sign</button>
        <button name="square" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>x<sup>2</sup></button>
        <button name="squareroot" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>√x</button>
        <button name="scintific" className="calci-button theme-button" onClick={e => this.props.onClick(e.target.name)}>sci</button><br />
      </div>
    );
  }

}


export default Keypad;